<?php
date_default_timezone_set('America/Sao_Paulo');
/**
 * Retorna numeral para ser gravado no banco de dados ou realizar operações matemáticas
 *
 * @param string $valor        = 1.000,00;
 * @param bool   $retorno_nulo = true;
 *
 * @return number 1000.00
 */
function moedaParaNumero($valor, $retorno_nulo=true)
{
    if (is_null($valor) || strlen($valor) == 0) {
        if ($retorno_nulo) {
            return null;
        } else {
            return 0;
        }
    }

    $valor = str_replace('.', '', $valor);
    $valor = str_replace(',', '.', $valor);

    return $valor;
}

function ajusteURL($url) 
{
    return preg_match('/(http)/mi', $url) ? $url : 'http://'.$url;
}

/**
 * Retorna o numero passado com o formato de moeda no BR. Caso $preffix seja true, retorna com R$ na frente;
 *
 * @param number  $valor        = 1000.00;
 * @param boolean $preffix      = false;
 * @param bool    $retorno_nulo = true;
 *
 * @return string 1.000,00
 */
function numeroParaMoeda($valor, $preffix=false, $retorno_nulo=true)
{
    if (is_null($valor) || strlen($valor) == 0) {
        if ($retorno_nulo) {
            return null;
        } else {
            $valor = 0;
        }
    }

    if ($preffix) {
        //return "R$ ".str_replace('.', ',', $valor);
        return "R$ ".number_format($valor, 2, ',', '.');
    } else {
        //return str_replace('.', ',', $valor);
        return number_format($valor, 2, ',', '.');
    }
}

/**
 * Retona um numeral preparado para banco de dados ou operações matemáticas;
 *
 * @param string $percente     = 1,00;
 * @param bool   $retorno_nulo = true;
 *
 * @return number 1.00
 */
function percenteParaNumero($percente, $retorno_nulo=true)
{
    $percente = str_replace('%', '', $percente);
    if (is_null($percente) || strlen($percente) == 0) {
        if ($retorno_nulo) {
            return null;
        } else {
            $percente = 0;
        }
    }

    return str_replace(',', '.', $percente);
}

/**
 * Retorna o numero com o % no final caso $suffix seja true;
 *
 * @param number  $percente     = 1.00;
 * @param boolean $suffix       = false;
 * @param bool    $retorno_nulo = true;
 *
 * @return string 1,00;
 */
function numeroParaPercente($percente, $suffix=false, $retorno_nulo=true)
{
    if (is_null($percente) || strlen($percente) == 0) {
        if ($retorno_nulo) {
            return null;
        } else {
            $percente = 0;
        }
    }

    if ($suffix) {
        return str_replace('.', ',', $percente)." %";
    } else {
        return str_replace('.', ',', $percente);
    }
}
/**
 * Retorna o ícone padrão do FontAwensome dependendo do tipo
 *
 * @param string $item 'Editar' | 'Cadastrar'
 *
 * @return string
 */
function icones($item)
{
    switch ($item) {
    case 'Editar':
        return 'fas fa-edit';
        break;
    case 'Vizualizar':
        return 'fas fa-glasses';
        break;
    case 'Visualizar':
        return 'fas fa-glasses';
        break;
    case 'Excluir':
        return 'far fa-trash-alt';
        break;
    case 'Favoritar':
        return 'far fa-star';
        break;
    case 'Duplicar':
        return 'fa fa-clone';
        break;
    case 'Destacar':
        return 'fa fa-bookmark ';
        break;
    case 'Ativar':
        return 'far fa-check-circle';
        break;
    case 'Inativar':
        return 'fas fa-times-circle' ;
        break;
    case 'Cancelar':
        return 'fas fa-times' ;
        break;
    }
}

/**
 * Retorna o endereço BASE de Upload do usuário logado
 *
 * @return string /assets/uploads/1/
 */
function getBaseUploadPath()
{
    $CI =& get_instance();
    $sessao = $CI->session->userdata('dados');
    if (!$sessao) {
        return BASE_UPLOAD_PATH;
    }
    
    $path = BASE_UPLOAD_PATH . $CI->cache->get($sessao['idLogado'])->id_assinatura . '/';
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    return $path;
}

/**
 * Retorna o endereço do site + pasta de uplaod do usuário logado
 *
 * @return string 'http://127.0.0.1/assets/uploads/1/'
 */
function getUploadPath()
{
    $CI =& get_instance();
    $sessao = $CI->session->userdata('dados');
    if (!$sessao) {
        return UPLOAD_PATH;
    }

    return UPLOAD_PATH . $CI->cache->get($sessao['idLogado'])->idEmpresa . '/';
}

/**
 * Retorna se o usuário é admin
 *
 * @return bool
 */
function isAdmin($sessao)
{
    return $sessao['dados_usuario']->master == 1;
}

/**
 * Se passar a data e formato de banco de dados ele retorna no formato brasileiro;
 * Se a data for '' e $hoje receber true, ele retorna a data atual no formato brasilheiro;
 *
 * @param string $data   = 03-11-2018 11:05:12 || $data = 1562341494;
 * @param bool   $hora   = false || true;
 * @param bool   $separa = false || true
 *
 * @return string 11/03/2018 || 11/03/2018 11:05:12
 */
function dataView($data = '', $hora = false, $separa = false)
{
    if ($data==='') {
        return date('d/m/Y');
    }

    if (preg_match('/-/', $data) && $data != '0000-00-00') {
        $timestamp = strtotime($data);
    } elseif (!is_numeric($data)) {
        return '';
    }
    
    if ((preg_match('/:/', $data) && $hora) || (is_numeric($data) && $hora)) {
        if (!$separa) {
            return date('d/m/Y H:i:s', $timestamp);
        } else {
            return array(
                'data'=>date('d/m/Y', $timestamp),
                'hora'=>date('H:i:s', $timestamp)
            );
        }
    } else {
        return date('d/m/Y', $timestamp);
    }
}

/**
 * Se passar a data e formato de banco de dados ele retorna no formato brasileiro;
 * Se a data for '' e $hoje receber true, ele retorna a data atual no formato brasilheiro;
 *
 * @param string $data   = 11/03/2018 11:05:12 || $data = 1562341494
 * @param bool   $hora   = false || true
 * @param bool   $separa = false || true
 *
 * @return string 03-11-2018 || 03-11-2018 11:05:12 || array('data'=>'03-01-2018' , 'hora'=> '11:05:12')
 */
function dataHoraBanco($data = '', $hora = true, $separa = false)
{
    if ($data==='') {
        return date('d/m/Y');
    }

    if (preg_match('@/@', $data)) {
        $timestamp = strtotime(str_replace('/', '-', $data));
    } elseif (!is_numeric($data)) {
        return '';
    } else {
        $timestamp = $data;
    }

    if ((preg_match('/:/', $data) && $hora) || (is_numeric($data) && $hora)) {
        if (!$separa) {
            return date('Y-m-d H:i:s', $timestamp);
        } else {
            return array(
                'data'=>date('Y-m-d', $timestamp),
                'hora'=>date('H:i:s', $timestamp)
            );
        }
    } else {
        return date('Y-m-d', $timestamp);
    }
}

/**
 * Limpa caracteres epeciais de uma string
 *
 * @param string $string 
 *
 * @return string
 */
function limpaString($string)
{
    $retira = array(' ','-','/','.','(',')',',');
    return str_replace($retira, "", $string);
}

/**
 * $tipo = 1, CPF
 * $tipo = 2, CNPJ
 *
 * @param string $numero 
 * @param int    $tipo 
 * @param bool   $semformatacao = true, devolve sem máscara com zeros a esquerda
 *
 * @return string 123.456.789-89 |
 */
function mascaraCnpjCpf($numero, $tipo = null, $semformatacao=false)
{
    $retorno = '';
    $var_aux = '';
    $retira = array(' ','-','/','.','(',')');
    $var_limpa = str_replace($retira, "", $numero);

    if ($var_limpa!='') {
        $tamanho = (!is_null($tipo) ? ($tipo==1 ? 11 : 14) : strlen($var_limpa)) ;
        $var_aux = str_pad($var_limpa, $tamanho, "0", STR_PAD_LEFT);
        if (!$semformatacao) {
            if ($tamanho==11) {   //cpf

                $retorno = substr($var_aux, 0, 3);
                $retorno.= '.'.substr($var_aux, 3, 3);
                $retorno.= '.'.substr($var_aux, 6, 3);
                $retorno.= '-'.substr($var_aux, -2);
            } else {      // cnpj

                $retorno = substr($var_aux, 0, 2);
                $retorno.= '.'.substr($var_aux, 2, 3);
                $retorno.= '.'.substr($var_aux, 5, 3);
                $retorno.= '/'.substr($var_aux, 8, 4);
                $retorno.= '-'.substr($var_aux, -2);
            }
        }
    }
    return $retorno;
}
/**
 * Por padrão, retorna o número com a mascara aplicada
 * Pode passar um com a mascara e removerla, habilitando $semformatacao
 *
 * @param inter $numero             = 2134567890
 * @param bool  $numerocomddd       = true || false
 * @param bool  $retornadddseparado = false || true
 * @param bool  $semformatacao      = false || true
 *
 * @return string  (12) 3456-7890 || array('ddd'=>'(12)', 'telefone'=>'3456-7890')
 */
function mascaraTelefone($numero, $numerocomddd=true, $retornadddseparado=false, $semformatacao=false)
{
    $retorno = '';
    $var_aux = '';
    $ddd = '';
    $telefone='';
    $retira = array(' ','-','/','.','(',')');
    $var_limpa = str_replace($retira, "", $numero);
    $var_aux = $var_limpa;

    for ($i=0;$i<strlen($var_limpa);$i++) {   // retira zeros a esquerda
        if (substr($var_limpa, $i, 1)!='0') {
            $var_aux = substr($var_limpa, $i);
            break;
        }
    }

    $tamanho = strlen($var_aux);

    if ($tamanho>0) {
        if (!$numerocomddd) {
            $telefone = substr($var_aux, 0, $tamanho-4);
            $telefone .=($semformatacao ? '' : '-').substr($var_aux, $tamanho-4);
        } else {
            $ddd = ($semformatacao ? '' : '(').substr($var_aux, 0, 2).($semformatacao ? '' : ')');
            $telefone = ($semformatacao ? '' : ' ').substr($var_aux, 2, ($tamanho-4)-2);
            $telefone .= ($semformatacao ? '' : '-').substr($var_aux, ($tamanho-4));
        }
    }

    if ($retornadddseparado) {
        $retorno = array('ddd'=>$ddd, 'telefone'=>$telefone);
    } else {
        $retorno = $ddd.$telefone;
    }

    return $retorno;
}

/**
 * Se passar a data e formato de brasileiro ele retorna no formato do banco de dados;
 * Se $hoje receber true, ele retorna a data atual no formato banco de dados;
 *
 * @param string  $data = 11/03/2018;
 * @param boolean $hoje = false;
 *
 * @return date 03-11-2018;
 */
function dataBanco($data, $hoje=false)
{
    if ($hoje) {
        return date('Y-m-d');
    }
    return preg_match('@/@', $data) ? date('Y-m-d', strtotime(str_replace('/', '-', $data))) : null;
}

/**
 * Soma dias a uma data
 *
 * @param string         $data 
 * @param string|integer $dias 
 *
 * @return string
 */
function somarDias($data = '', $dias = 0)
{
    if ($data=='') {
        $data = date('Y-m-d');
    }
    return date('Y-m-d', strtotime($data.' +'.$dias.' day'));
}

/**
 * Diminui dias de uma data
 *
 * @param string         $data 
 * @param string|integer $dias 
 *
 * @return string
 */
function subtrairDias($data = '', $dias = 0)
{
    if ($data=='') {
        $data = date('Y-m-d');
    }
    return date('Y-m-d', strtotime($data.' -'.$dias.' day'));
}

/**
 * Padroniza os Labels das Datatables, se receber 1 retorna Sim com formatação de Span verde, quaqluer valor diferente retorna Não em vermelho
 *
 * @param number $ativo 
 *
 * @return string
 */
function labelAtivo($ativo)
{
    return $ativo == 1 ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>';
}

/**
 * Limpa caracteres especiais para emissão de documentos Fiscais
 *
 * @param string $nome String com o valor que deve ser removido os caracteres 
 * 
 * @return void
 */
function limparFiscal($nome) 
{
    return preg_replace('/"|\'/', "", $nome);
}

/**
 * Padroniza os Labels das Datatables, se receber 1 retorna Sim com formatação de Span verde, quaqluer valor diferente retorna Não em vermelho
 *
 * @param number $ativo 
 *
 * @return string
 */
function labelStatus($ativo, $array = array(0=>array('error'=>'Não'),1=>array('success'=>'Sim')))
{
    $each = $array[$ativo];
    foreach ($each as $key=>$value) {
        return ($key != 'error' ? ($key =='success' ? '<span class="label label-success">'.$value.'</span>' : '<span class="label label-warning">'.$value.'</span>') : '<span class="label label-danger">'.$value.'</span>');
    } 
    
}

/**
 * Retorna o valor forçando (String)
 *
 * @param int $value = 1
 *
 * @return '1'
 */
function convertstring($value)
{
    return (string)$value;
}

/**
 * Retorna um String randomica de 10 Caracteres
 *
 * @param int $length = 10
 *
 * @return 'dhsaljkdaskbcnzxncsf4'
 */
function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * Retorna a data que vem da API da vindi em formatao de banco
 *
 * @param string $data = '25-12-2018T11:15:20.000-03:00'
 * @param int    $days = 5 Quantidade de dias que vai ser somado a data
 *
 * @return string '2018-12-25 11:15:20'
 */
function dataVindi($data, $days = '')
{
    return date('Y-m-d H:i:s', strtotime(str_replace('T', ' ', str_replace('.000-03:00', "", $data)).' '.$days));
}

/**
 * Se passar uma extensão de arquivo, ele retorna o ícone do FontAwensome compatível;
 * Por padrão retorna fa fa-file
 * Precisa do FontAwensome >=5.0
 *
 * @param string  $exten = arquivo.pdf | pdf;
 * @param boolean $nome  = true | false;
 *
 * @return string fa fa-file-pdf;
 */
function iconeExtensao($exten, $nome=false)
{
    if ($nome) {
        $exten = explode('.', $exten)[1];
    }
   
    switch ($exten) {
    case 'pdf':
        return 'fa fa-file-pdf';
        break;
    case 'jpg':
        return 'fa fa-file-image';
        break;
    case 'docx':
        return 'fa fa-file-word';
        break;
    case 'doc':
        return 'fa fa-file-word';
        break;
    case 'xls':
        return 'fa fa-file-excel';
        break;
    case 'xlsx':
        return 'fa fa-file-excel';
        break;
    case 'ppt':
        return 'fa fa-file-powerpoint';
        break;
    case 'pptx':
        return 'fa fa-file-powerpoint';
        break;
    case 'zip':
        return 'fa fa-file-archive';
        break;
    case 'rar':
        return 'fa fa-file-archive';
        break;
    case 'mp4':
        return 'fa fa-file-video';
        break;
    case 'avi':
        return 'fa fa-file-video';
        break;
    default:
        return "fa fa-file";
    }
}

/**
 * Retorna o valor mascarado com o espelho
 *
 * @param string $mascara = '###.###.###-####'
 * @param string $string  = '12345678910'
 * @param bool   $os      = false
 *
 * @return string '123.456.789-10'
 */
function mascara_string($mascara, $string, $os=false)
{
    if ($string != null && $string != '') {
        $string = str_replace(" ", "", $string);

        for ($i=0;$i<strlen($string);$i++) {
            $mascara[strpos($mascara, "#")] = $string[$i];
        }

        if ($os) {
            return str_replace('#', '', $mascara);
        }

        return $mascara;
    } else {
        return null;
    }
}

/**
 * Retorna a String com JavaScript para o modal de informações
 *
 * @param string $msg = 'Olá, tudo bem?'
 *
 * @return string
 */
function Span_info($msg)
{
    return '<span style="color:#01579b; cursor: pointer;" class="fa fa-info-circle" onclick="pinfo(\''.str_replace('\n', '', addslashes($msg)).'\')"></span>';
}

/**
 * Retorna um TimeStamp de uma Data em PT-BR
 *
 * @param string $data 10/05/2019
 * 
 * @return int 
 */
function geraTimestamp($data)
{
    if (preg_match('/:/', $data)) {
        $datas = explode(' ', $data);
    } else {
        $datas[0] = $data;
    }

    $partes = explode('/', $datas[0]);

    if (!preg_match('/-/', $datas[0])) {
        return mktime(0, 0, 0, $datas[0]);
    } else {
        return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
    }
}

/**
 * Verificar se o Dominio existe
 *
 * @param string $dominio 
 *
 * @return bool
 */
function Dominio_existe($dominio)
{
    if (checkdnsrr($dominio, 'ANY') && gethostbyname($dominio) != $dominio) {
        return true;
    } else {
        return false;
    }
}

/**
 * Procura um valor em um array multidimensional e retorna sua posição
 *
 * @param array  $array  = array(array('nome'=>'virgilio'),array('nome'=>'luis'))
 * @param string $value  = 'luis'
 * @param string $column = 'nome'
 *
 * @return integer $key  = 2
 */
function Help_Array_Search_for($array, $value, $column)
{
    foreach ($array as $key => $val) {
        if (is_object($val) && (isset($val->{$column}) && $val->{$column} == $value) || (is_array($val) && isset($val[$column]) && $val[$column] == $value)) {
            return $key;
        }
    }
    return null;
}

/**
 * Recebe a lista de emails e retorna o objeto preparado para ser transformado em JSON e gravado no banco
 *
 * @param array  $tipos       = array('1','2','2') 1 - Pessoal 2 - Empresarial
 * @param array  $numeros     = array('2127631052','21123456789',hoje@teste.com)
 * @param array  $contatos    = array('Maria','João','Virgilio')
 * @param array  $id_entidade =
 * @param string $nome_id     = Nome do campo
 *
 * @return array
 */
function Prepara_telefones($tipos, $numeros, $contatos, $id_entidade=null, $nome_id='')
{
    // $id_entidade ==> é o id do cliente / fornecedor / transportadora caso $e_tabela = true
    $lista_telefones = array();
    $search = array(' ','-', '.', '/', '_', '[', ']', '(', ')', '=', '+', '*', '^', '~', '´', '`', '{', '}', ';');
    if ($numeros) {
        foreach ($numeros as $i => $value) {
            $idTipo = $tipos[$i];
            $numero = $value;
            $contato = $contatos[$i];
            if ($numero != "") {
                if ($nome_id!='') {
                    $lista_telefones[] = array(
                        'idTipo' => $idTipo,
                        'telefone' => str_replace($search, '', $numero),
                        'contato_telefone'=> $contato,
                        "$nome_id" => $id_entidade
                    );
                } else {
                    $lista_telefones[] = array(
                        'id'=>$i+1,
                        'idTipo' => $idTipo,
                        'telefone' => str_replace($search, '', $numero),
                        'contato_telefone'=> $contato
                    );
                }
            }
        }
    }
    return $lista_telefones;
}

/**
 * Recebe a lista de emails e retorna o objeto preparado para ser transformado em JSON e gravado no banco
 *
 * @param array  $tipos       = array('1','2','2') 1 - Pessoal 2 - Empresarial
 * @param array  $emails      = array('ex@ex.com','teste@teste.com',hoje@teste.com)
 * @param array  $envia_nfes  = array()
 * @param array  $id_entidade =
 * @param string $nome_id     = Nome co campo
 *
 * @return array
 */
function Prepara_emails($tipos, $emails, $envia_nfes = null, $id_entidade=null, $nome_id='')
{
    // $id_entidade ==> é o id do cliente / fornecedor / transportadora caso $e_tabela = true
    $lista_emails = array();
    if ($emails) {
        foreach ($emails as $i => $value) {
            $idTipo = $tipos[$i];
            $email = $value;
            if (!is_null($envia_nfes)) {
                $envia_nfe = $envia_nfes[$i];
            }
            if ($email != "") {
                if ($nome_id!='') {
                    if (!is_null($envia_nfes)) {
                        $lista_emails[$i] = array(
                            'idTipo' => $idTipo,
                            'email' => $value,
                            'envia_nfe'=>$envia_nfe,
                            "$nome_id" => $id_entidade
                        );
                    } else {
                        $lista_emails[$i] = array(
                            'idTipo' => $idTipo,
                            'email' => $value,
                            "$nome_id" => $id_entidade
                        );
                    }
                } else {
                    if (!is_null($envia_nfes)) {
                        $lista_emails[$i] = array(
                            'id'=>$i+1,
                            'idTipo' => $idTipo,
                            'email' => $value, 'envia_nfe'=>$envia_nfe
                        );
                    } else {
                        $lista_emails[$i] = array(
                            'id'=>$i+1,
                            'idTipo' => $idTipo,
                            'email' => $value
                        );
                    }
                }
            }
        }
    }

    return $lista_emails;
}
