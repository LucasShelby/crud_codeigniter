<?php

class Usuarios_model extends CI_Model
{
    public $nome;
    public $email;
    public $fone;
    public $senha;
    public $perfil;
    public $ativo;

    public function __construct()
    {
        parent:: __construct();
        $this->table = 'geek';
    }

    function gerarArrayDados()
    {
        $dados = array(
            "nome" => $this->nome,
            "email" => $this->email,
            "fone" => $this->fone,
            "senha" => $this->senha,
            "perfil" => $this->perfil,
            "ativo" => $this->ativo,
        );

        return $dados;
    }

    public function inserir()
    {        
        $dados = gerarArrayDados();
        return $this->db->insert($this->table,$dados);    
    }

    public function update($id)
    {
        $dados = gerarArrayDados();
        return $this->db->update($this->table, $dados, array('id' => $id));
    }

    public function lista()
    {
        $query = $this->db->get('geek');
        return $query->result();
    }

    public function get($id)
    {   
        $this->db->from($this->table);
        $this->db->where(
            array(
                'id'=>$id
            )
        );

        return $this->db->get()->row();
    }

    

}