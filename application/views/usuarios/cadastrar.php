<!DOCTYPE html>
<html lang="en">
<head>
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php //var_dump(validation_errors()); ?>
    <form id="formulario" method="post" role="form">
        <legend>Usuarios</legend>
        <div id="erros"></div>
        <div class="form-group">
            <br>
            <label for="">Nome</label>
            <input name="nome" type="text" value="<?php echo isset($usuario) ? $usuario->nome : set_value('nome')?>" class="form-control" id="nome" placeholder="Nome de Usuario">
        </div>
        <div class="form-group">
            <br>
            <label for="">E-mail</label>
            <input name="email" type="text" value="<?php echo isset($usuario) ? $usuario->email : set_value('email')?>" class="form-control" id="email" placeholder="E-mail">
        </div>
        <div class="form-group">
            <br>
            <label for="">Telefone</label>
            <input name="fone" type="text" value="<?php echo isset($usuario) ? $usuario->fone : set_value('fone')?>" class="form-control" id="fone" placeholder="Telefone">
        </div>
        <div class="form-group">
            <br>
            <label for="">Senha</label>
            <input name="senha" type="text" value="<?php echo isset($usuario) ? $usuario->senha : set_value('senha')?>" class="form-control" id="senha" placeholder="Senha para login">
        </div>
        <div class="form-group">
            <br>
            <label>ATIVO/DESATIVADO</label>
            <input type="checkbox" name="ativo" value="1" <?php echo set_checkbox('ativo', '1'); ?>>Ativo<br>
        </div>
        <br>
        <!--<button type="submit" class="btn btn-primary">Enviar</button>-->
    </form>
    <br>
    <div>
        <button data-toggle="tooltip" data-html="true" data-type="save" title="Salvar" id="botCadastrarFloat" href="#" class="">
        SALVAR
        </button>
    </div>
</body>

<script type ="text/javascript">

function enviarDados(btn, config = {}){
    const defaultConfig = {
        url: '<?php echo current_url()?>',
        data: $('#formulario').serialize(),
        formData: '',
        beforeError: function() {

        },
        afterError: function() {

        },
        beforeSuccess: function() {

        }
    }

    configF = Object.assign(defaultConfig,config)

    errorsDiv = $('#erros');

    ajaxConfig = {
            url: configF.url,
            method: 'POST',
            dataType: 'JSON',
            success: function(data){
                if (data.error!==undefined && data.error !=='') {

                    configF.beforeError(data)
                
                    if (errorsDiv.html() === undefined) {//Verifica se a DIV de erro existe
                        console.error(data.error)

                    } else {
                        

                        window.scrollTo(0, 0);
                        errorsDiv.html(
                            '<div class="alert alert-danger">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                data.error +
                            '</div>'
                        );

                    }

                    configF.afterError(data)
                
                }else{
                    
                    configF.beforeSuccess(data)

                    document.location.href='<?=base_url($this->router->fetch_class())?>';
                }  
                    
                if(data.ids_erros !== undefined){ //Marca de vermelho os campos que estão com erro
                    inputError(data.ids_erros);
                }     
            },
            error: function(xhr, textStatus, errorThrown) {   
                
                configF.beforeError()
                
                console.error(xhr.responseText);
                configF.afterError()
                
            }
        };

        if(configF.formData=='') {
            ajaxConfig.data = configF.data;
        } else {
            ajaxConfig.data = new FormData($('#'+configF.formData)[0]);
            ajaxConfig.conttentType = false;
            ajaxConfig.processData = false;
        }

        $.ajax(ajaxConfig);
    }

    $('#botCadastrarFloat').click(function() {
        console.log('data')
        ob = {
            beforeError: function(){
                console.log('beforeError')
            },
            afterError: function(){
                console.log('afterError')
            },
            beforeSuccess: function(){
                console.log('beforeSucces')
            }
        }

        enviarDados(this, ob)
    });
    
</script>

</html>


