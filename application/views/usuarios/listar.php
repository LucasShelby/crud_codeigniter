<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table border="1" align="center">
        <br><br><br><br><tr>
            <th>ID</th>
            <th>Nome</th>
            <th>E-Mail</th>
            <th>Telefone</th>
            <th>Senha</th>
            <th>Ativo</th>
            <th>Ação</th>
        </tr>
        <?php foreach ($usuarios as $us) {

         ?>
        <tr>
            <td><?php echo $us->id; ?></td>
            <td><?php echo $us->nome; ?></td>
            <td><?php echo $us->email; ?></td>
            <td><?php echo $us->fone; ?></td>
            <td><?php echo $us->senha; ?></td>
            <td><?php echo $us->ativo ==1 ? 'SIM' : 'NÃO'; ?></td>
            <td>
                <a id="editar" value="2" href='<?php echo BASE_URL.$this->router->fetch_class();?>/editar/<?php echo $us->id; ?>' class="btn btn-primary" >EDITAR</a>
                <a id="desativar" value="3" href='<?php echo BASE_URL.$this->router->fetch_class();?>/action/usuario.php?id=<?php //echo $value['id'].($value['ativo'] == 1 ?'&action=off' : '&action=on') ?>'" class="btn btn-primary" ><?php //echo $value['ativo']==1 ? "DESATIVAR" : "ATIVAR"; ?></a>
            </td>                        
        </tr>
    <?php } ?>

    </table>
    
</body>
</html>