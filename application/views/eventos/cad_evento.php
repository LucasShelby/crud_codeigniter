<?php
$menu = 'index';
include_once (__DIR__."/view/header.php");
?>
           <!-- Multi Column -->
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                CADASTRO DE EVENTO
                            </h2>
                            <div align="right" >
                                <br>
                                <button type="button" class="btn bg-green waves-effect">
                                    <i class="material-icons">save</i>
                                    <span>SAVE</span>
                                </button>
                            </div>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <label for="nome" class="form-label">Nome do Evento</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                        <input type="text" id="nome" class="form-control">

                                        </div>
                                    </div>
                                </div>
                            </div>
 
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="data" class="form-label">Data</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="data" class="form-control">
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="hora_inicio" class="form-label">Horário de inicio</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="time" id="hora_inicio" class="form-control">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <label for="hora_fim" class="form-label">Horário de Termino</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="time" id="hora_fim" class="form-control" >
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="local" class="form-label">Local</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="local" class="form-control">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                <label for="valor_entrada" class="form-label">Valor da Entrada</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="valor_entrada" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-md-3">
                                <label for="qtd_esperada" class="form-label">Quantidade Esperada</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="qtd_esperada" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Multi Column -->
<?php
    include_once (__DIR__.'/view/footer.php');
?>
