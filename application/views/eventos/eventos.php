<?php
$menu = 'eventos';
include_once __DIR__.'/view/header.php';

?>

<!-- Button Sizes -->
<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                CADASTRE E GERENCIE OS EVENTOS
                                <small>You can resize the buttons</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix demo-button-sizes">
                            <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                            <input class="btn btn-block btn-lg bg-pink waves-effect" id="Cadastrar" value="Cadastrar" onclick="window.location.href='cad_evento.php'" type="button"></input>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Button Sizes -->

<?php
include_once __DIR__.'/view/footer.php';
?>