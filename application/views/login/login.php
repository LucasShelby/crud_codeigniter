﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Geek Universe | Login</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><b>Geek Universe</b></a>
            <small>LOGIN</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST">
                    <div class="msg">Já Cadastrado? Se indentifique e entre!</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="email" placeholder="E-mail" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="senha" placeholder="Senha" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Lembrar Senha</label>
                        </div>
                        <div class="col-xs-4">
                            <input class="btn btn-block btn-lg bg-pink waves-effect" onclick="logar()" id="Logar" value="Logar" type="button"></inupt>
                            <input class="btn btn-block bg-pink waves-effect" name="action" value="logar" type="hidden"></input>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="sign-up.html">CADASTRE-SE AQUI!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot-password.html">LEMBRAR SENHA?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/examples/sign-in.js"></script>
    <script>
        $(document).keypress(function(e){
            if(e.which == 13&&$('#Logar').attr("disabled")===undefined) $('#Logar').click();
        });

        function logar(){
            $("#Logar").prop("disable", true);
            $.ajax({
                type: "POST",
                url: "action/login.php",
                data: $('#sign_in').serialize(),
                dataType: 'JSON',
                success: function(data){
                    $("#Logar").prop("disabled", false);
                    if(data.logar!==undefined){
                        window.location.href="/geek";
                    }
                },
                error: function(data){
                    alert(data.responseText);
                    $("#Logar").prop("disabled", false);
                }
            });
        }
    </script>
</body>

</html>