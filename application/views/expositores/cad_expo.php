<?php
$menu = 'index';
include_once (__DIR__."/view/header.php");
?>

            <!-- CADASTRO DE EXPOSIT -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        
                        <div class="header">
                            <h2> CADASTRO DE EXPOSITOR </h2>
                            <div align="right" >
                                <br>
                                <button type="button" class="btn bg-green waves-effect">
                                    <i class="material-icons">save</i>
                                    <span>SAVE</span>
                                </button>
                            </div>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form id="cad_expo">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <label for="nome_loja" class="form-label">Nome da Loja</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name='nome_loja' id="nome_loja" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-md-5">
                                        <label for="representante"  class="form-label">Representante</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name='representante' id="representante" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label for="telefone" class="form-label">Telefone</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name='telefone' id="telefone" class="form-control">
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="col-md-4">
                                        <label for="email" class="form-label">E-Mail</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name='email' id="email" class="form-control">
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                                <input value ="new" name="action" type="hidden"/>
                            </form>
                        </div>                         
                    </div>
                </div>
            </div>
            <!-- #END# CADASTRO DE EXPOSITOR -->

            <!-- Radio -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                PRODUTOS
                                <small>Taken from <a href="http://materializecss.com/" target="_blank">materializecss.com</a></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form id="produtos">
                                <h2 class="card-inside-title">
                                    Marque o que encontrar nesta loja
                                    <small>You can use material design colors which examples are <code>.radio-col-pink, .radio-col-cyan</code> class</small>
                                </h2>
                                <div class="row">
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <input name="alimentos" type="checkbox" id="checkbox_1" class="filled-in" />
                                        <label for="checkbox_1">ALIMENTOS</label>
                                    </div>
                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <input name="acessorios" type="checkbox" id="checkbox_2" class="filled-in" checked />
                                        <label for="checkbox_2">ACESSÓRIOS</label>
                                    </div>
                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <input name="brinquedos" type="checkbox" id="checkbox_3" class="filled-in" />
                                        <label for="checkbox_3">BRINQUEDOS</label>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <input name="casa" type="checkbox" id="checkbox_4" class="filled-in" />
                                        <label for="checkbox_4">CASA</label>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <input name="colecionaveis" type="checkbox" id="checkbox_5" class="filled-in" />
                                        <label for="checkbox_5">COLECIONÁVEIS</label>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <input name="escritorio" type="checkbox" id="checkbox_6" class="filled-in" />
                                        <label for="checkbox_6">ESCRITÓRIO</label>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <input name="jogos" type="checkbox" id="checkbox_7" class="filled-in" />
                                        <label for="checkbox_7">JOGOS</label>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <input name="vestuario" type="checkbox" id="checkbox_8" class="filled-in" />
                                        <label for="checkbox_8">VESTURÁRIO</label>
                                    </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Radio -->

            <input class="btn btn-block btn-lg bg-pink waves-effect" id="Cadastrar" value="Cadastrar" onclick="cadastrar()" type="button">
           

                
 

            <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/examples/sign-up.js"></script>

    <script>
        $(document).keypress(function(e) {
            if(e.which == 13&&$('#Cadastrar').attr("disabled")===undefined) $('#Cadastrar').click();
        });

        function cadastrar(){
            $("#Cadastrar").prop("disabled", true);
            $.ajax({
                type: "POST",
                url: "action/expositores.php",
                data: $('#cad_expo').serialize()+'&'+$('#produtos').serialize(),
                dataType:'JSON',
                success: function(data){
                    resposta = JSON.parse(data);
                    if(resposta){
                        window.location.href='index.php'
                    }
                    $("#Cadastrar").prop("disabled", false);
                },
                error: function(data){
                    alert(data.responseText);
                    $("#Cadastrar").prop("disabled", false);
                }
            });
        }
    </script>
<?php
    include_once (__DIR__.'/view/footer.php');
?>