<?php
class MY_Form_validation extends CI_Form_validation{

	function __construct($config = array()){
		parent::__construct($config);
	}

	function validarCPF($cpf)
    {
        $cpf = "$cpf";
        if (strpos($cpf, "-") !== false)
        {
          $cpf = str_replace("-", "", $cpf);
        }
        if (strpos($cpf, ".") !== false)
        {
            $cpf = str_replace(".", "", $cpf);
        }
        $sum = 0;
        $cpf = str_split( $cpf );
        $cpftrueverifier = array();
        $cpfnumbers = array_splice( $cpf , 0, 9 );
        $cpfdefault = array(10, 9, 8, 7, 6, 5, 4, 3, 2);
        for ( $i = 0; $i <= 8; $i++ )
        {
            $sum += $cpfnumbers[$i]*$cpfdefault[$i];
        }
        $sumresult = $sum % 11;  
        if ( $sumresult < 2 )
        {
            $cpftrueverifier[0] = 0;
        }
        else
        {
            $cpftrueverifier[0] = 11-$sumresult;
        }
        $sum = 0;
        $cpfdefault = array(11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
        $cpfnumbers[9] = $cpftrueverifier[0];
        for ( $i = 0; $i <= 9; $i++ )
        {
            $sum += $cpfnumbers[$i]*$cpfdefault[$i];
        }
        $sumresult = $sum % 11;
        if ( $sumresult < 2 )
        {
            $cpftrueverifier[1] = 0;
        }
        else
        {
            $cpftrueverifier[1] = 11 - $sumresult;
        }
        $returner = false;

        if ( $cpf == $cpftrueverifier )
        {
            $returner = true;
        }


        $cpfver = array_merge($cpfnumbers, $cpf);

        if ( count(array_unique($cpfver)) == 1 || $cpfver == array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0) )

        {

            $returner = false;

        }

        $this->set_message('validarCPF', 'Informe um CPF válido no campo %s');

        return $returner;
    
    }

	function validarCNPJ($cnpj)
    {
        $cnpj = "$cnpj";
        if((preg_match('/./',$cnpj)<2)&&!preg_match('/-/',$cnpj)){
            $this->set_message('validarCNPJ', 'Informe um CNPJ válido no campo %s');
            return false;
        }

        if (strpos($cnpj, "/") !== false)
        {
          $cnpj = str_replace("/", "", $cnpj);
        }
        if (strpos($cnpj, ".") !== false)
        {
            $cnpj = str_replace(".", "", $cnpj);
        }
        if (strpos($cnpj, "-") !== false)
        {
            $cnpj = str_replace("-", "", $cnpj);
        }

        $sum = 0;
        $cnpj = str_split( $cnpj );
        $cnpjtrueverifier = array();
        $cnpjnumbers = array_splice( $cnpj , 0, 12 );
        $cnpjdefault = array(5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
        for ( $i = 0; $i <= 11; $i++ )
        {
            $sum += $cnpjnumbers[$i]*$cnpjdefault[$i];
        }
        $sumresult = $sum % 11;  
        if ( $sumresult == 0 || $sumresult == 1 )
        {
            $cnpjtrueverifier[0] = 0;
        }
        else
        {
            $cnpjtrueverifier[0] = 11-$sumresult;
        }
        $sum = 0;
        $cnpjdefault = array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
        $cnpjnumbers[12] = $cnpjtrueverifier[0];
        for ( $i = 0; $i <= 12; $i++ )
        {
            $sum += $cnpjnumbers[$i]*$cnpjdefault[$i];
        }
        $sumresult = $sum % 11;
        if ( $sumresult < 2 )
        {
            $cnpjtrueverifier[1] = 0;
        }
        else
        {
            $cnpjtrueverifier[1] = 11 - $sumresult;
        }
        $returner = false;

        if ( $cnpj == $cnpjtrueverifier )
        {
            $returner = true;
        }

        $cnpjver = array_merge($cnpjnumbers, $cnpj);

        if ( count(array_unique($cnpjver)) == 1 )

        {

            $returner = false;

        }

        $this->set_message('validarCNPJ', 'Informe um CNPJ válido no campo %s');

        return $returner;

    }

    function getGroupRule($group){
        if(!isset($this->_config_rules[$group])){
            return false;
        }
        return $this->_config_rules[$group];
    }

    function fildErros(){
        return $this->_error_array;
    }

}