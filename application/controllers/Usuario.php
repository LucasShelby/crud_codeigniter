<?php

class Usuario extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Usuarios_model');
    }

    public function index()
    {
        $dados['usuarios'] = $this->Usuarios_model->lista();
        $this->load->view('listar',$dados);
    }

    public function salvar()
    {
        
    }

    public function cadastrar()
    {
        $this->load->library('form_validation');
        $rules = $this->_regras_validacao();
        $this->form_validation->set_rules($rules);

        if($this->form_validation->run() == false) {
            
            $this->inputError();
            $this->load->view('form_cad');

        } else {
            $nome = $this->input->post('nome');
            $email = $this->input->post('email');
            $fone = $this->input->post('fone');
            $senha = $this->input->post('senha');
            $ativo = $this->input->post('ativo',0,1);
    
            $this->Usuarios_model->nome = $nome;
            $this->Usuarios_model->email = $email;
            $this->Usuarios_model->fone = $fone;
            $this->Usuarios_model->senha = $senha;
            $this->Usuarios_model->ativo = $ativo;
    
            $this->Usuarios_model->inserir();
        }

    }

    public function editar($id)
    {
        $this->load->library('form_validation');
        $rules = $this->_regras_validacao();
        $this->form_validation->set_rules($rules);

        if($this->form_validation->run() == false) {
            $this->InputError();
            $dados['usuario'] = $this->Usuarios_model->get($id);
            if (empty($dados['usuario'])) {
                header('Location:'.BASE_URL.$this->router->fetch_class());
            }
            $this->load->view('form_cad', $dados);
        } else {
            $nome = $this->input->post('nome');
            $email = $this->input->post('email');
            $fone = $this->input->post('fone');
            $senha = $this->input->post('senha');
            $ativo = $this->input->post('ativo', 0, 1);

            $this->Usuarios_model->nome = $nome;
            $this->Usuarios_model->email = $email;
            $this->Usuarios_model->fone = $fone;
            $this->Usuarios_model->senha = $senha;
            $this->Usuarios_model->ativo = $ativo;

            if ($this->Usuarios_model->update($id)) {
                echo json_encode(array('error'=>''));
            } else {
                echo json_encode(array('error'=>'Erro'));
            }

        }

    }

    private function _regras_validacao()
    {
        return array(
            array (
                'field' => 'nome',
                'label' => 'nome',
                'rules' => 'required',
            ),
            array (
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required',
            ),
            array (
                'field' => 'fone',
                'label' => 'fone',
                'rules' => 'required',
            ),
            array (
                'field' => 'senha',
                'label' => 'senha',
                'rules' => 'required',
            ),
            array (
                'field' => 'ativo',
                'label' => 'ativo',
                'rules' => '',
            ),
        );
    }

    private function inputError()
    {
        if (validation_errors() != '') {
            $fildErros = $this->form_validation->fildErros();
            foreach ($fildErros as $key=>$value) {
                $id_erros[] = array('id'=>$key);
            }

            echo json_encode(array('error' => validation_errors(),'ids_erros'=>$id_erros));
            exit();
        }
    }
}