<?php

class usuario_geek extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Usuarios_model');
    }

    /* function __construct(){
        $this->banco = new banco();
        $this->tabela = 'usuarios';

    } */
    function index()
    {
        $this->data['menu'] = 'usuarios';
        $this->data['view'] = 'usuarios/listar';
        $this->load->view('includes/header', $this->data);
        echo "Olá Usuário!";
        
    }


    function cadastrar($dados){
        if(!is_array($dados)){
            return false;
        }

        if($this->emailExiste($dados['email'])){
            echo "E-mail já cadastrado!";
            exit();
        }

        $result = $this->banco->insert($this->tabela,$dados);
        return $result;
    }

    function editar($dados,$campoWhere){
        if(!is_array($dados)){
            return false;
        }
        $result = $this->banco->update($this->tabela,$dados,$campoWhere);
        return $result;
    }

    function deletar($campoWhere){
        if(!is_array($campoWhere)){
            return false;
        }
        $result = $this->banco->delete($this->tabela,$campoWhere);
        return $result;
    }

    function buscaDados()
    {
        $this->load->view('cadastrar');


    }

    function emailExiste($email){
        $campoWhere = array(
            'email'=>$email
        );
        $result = $this->banco->select($this->tabela,$campoWhere);
        if(isset($result['nome'])){
            return true;
        }else{
            return false;
        }
    }

    function validarLogin($campoWhere){
        $result = $this->banco->select($this->tabela,$campoWhere);
        if(isset($result['nome'])&&isset($result['senha'])){
            return $result;
        }else{
            return false;
        }
    }

    function criarsessao($dadosUsuario){
        session_start();
        $_SESSION['nome'] = $dadosUsuario['nome'];
        $_SESSION['email'] = $dadosUsuario['email'];
        $_SESSION['tipo'] = $dadosUsuario['tipo'];
        $_SESSION['id'] = $dadosUsuario['id'];
    }

    function endSessao(){
        session_start();
        session_destroy();
    }

    function verifSessao(){
        if(isset($_SESSION['id'])){
            return true;
        }else{
            return false;
        }
    }
}


?>